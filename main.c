//main
//This is the main file and the master file for the oss/slave program
//Justin Henn
//3/05/17
//Assignment 3
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#include "msgqueue.h"
#define PERM (S_IRUSR | S_IWUSR)

  typedef struct {
   long mtype;
   char mtext[1024];
  } mymsg_t;

int* point;
int num_proc_for_alarm, shm_id_seconds, shm_id_nano_seconds;
int* seconds;
int* nano_seconds;
static int child_queueid, parent_queueid;

//kills all child processes
void kill_childs() {

  int y, z = num_proc_for_alarm;
  signal(SIGTERM, SIG_IGN);
  kill(0, SIGTERM);
  for(y = 0; y < num_proc_for_alarm; y++){

    if (wait(NULL) != -1) {

      z--;
    }
  }
}


//alarm handler

void ALARM_handler(int sig) {//Argument here is signal number

/*  int y;
  fprintf(stderr, "Reached alarm\n");
  for(y = 0; y < num_proc_for_alarm; y++)
    kill(point[y], SIGKILL); */


  kill_childs();

  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
   exit(0);
}

//Control C handler

void SIGINT_handler(int sig) {
  signal(sig, SIG_IGN);
  printf("Received Control C\n");
  kill_childs();
    if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit (1);
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    exit(1);
  }

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");
  exit(0);
}

//detach nd remove shared memory
int detachandremove(int shmid, void *shmaddr) {
   int error = 0;

   if (shmdt(shmaddr) == -1)
     error = errno;
   if ((shmctl(shmid, IPC_RMID, NULL) == -1) && !error)
     error = errno;
   if (!error)
      return 0;
   return -1;
}

/*int initqueue(int key, int queueid) {                     initialize the message queue
   queueid = msgget(key, PERM | IPC_CREAT);
   if (queueid == -1) {

      perror("msgget");
      exit(1);
   }
  return queueid;
}*/

/*int remmsgqueue(int queueid) {
   return msgctl(queueid, IPC_RMID, NULL);
}*/


int main (int argc, char **argv) {

  int opt;
  char* filename = NULL;
  int number_proc = 0, number_wait = 0, i, x, processes_made = 0;
  extern char* optarg;
  key_t key_seconds, key_nano_seconds, child_queue_key, parent_queue_key;
  key_seconds = 3423563;
  key_nano_seconds = 12345;
  child_queue_key = 85733;
  parent_queue_key = 922435;
  mymsg_t mymsg, sentmsg;
  FILE* logfile;

  while ((opt = getopt(argc, argv, "hs:l:t:")) != -1) { //this loop looks at the the arguments and processes them appropriately

    switch (opt) {
    case 'h':
      printf("h - help\nn - specify a value after n for the error code \nl - specify a file name after l for the logfile \ns - number of slave processes\nt - seconds when program should terminate\n");
      return 0;
    case 's':
      number_proc = atoi(optarg);
      break;
    case 'l':
      filename = optarg;
      break;
    case 't':
      number_wait  = atoi(optarg);
      break;
    }
  }

  if (filename == NULL) { //if no filename was givien as an argument

     filename = "test.out";
  }

  if (number_proc == 0) { //if no number of processes was given as argument

    number_proc = 4;
  }
  if (number_wait == 0) { //if no wait time was given as argument

    number_wait = 20;
  }

  if ((logfile = fopen(filename, "a+")) == NULL) { //check to see if it opened a file

      perror("File open error");
      return -1;
    }
  //get shared memory for seconds
  if ((shm_id_seconds = shmget(key_seconds, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((seconds = (int *)shmat(shm_id_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_seconds, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
   }

  //get shared memory for nano_seconds;
  if ((shm_id_nano_seconds = shmget(key_nano_seconds, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((nano_seconds = shmat(shm_id_nano_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_nano_seconds, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }

 //create queues

  child_queueid = initqueue(child_queue_key, child_queueid);

  parent_queueid = initqueue(parent_queue_key, parent_queueid);

 //initialize the shared memory with 0
  *nano_seconds = 0;
  *seconds = 0;

  processes_made = number_proc;
  pid_t pids[number_proc];
  pid_t child_pid;
  sprintf(mymsg.mtext, "This is proc %d", getpid());
  mymsg.mtype = 1;
  if(msgsnd(child_queueid, mymsg.mtext, 1024,0) == -1) {
    perror("Failed write");
    return 1;
  }
  //printf("%d\n", getpid());

  //create processes

  for(i=0; i<number_proc; i++) {
   child_pid = fork();

  //couldnt fork
    if (child_pid < 0) {

      printf("Could not fork!");
      return 1;
    }

   //Create array of process ids so can parent process can wait for all processes
    else if (child_pid > 0) {
      pids[i] = child_pid;
      point = pids;
      num_proc_for_alarm = number_proc;

    }

  //child process
    else  {

    //printf("in child %d\n", getpid());
   //   execl("./slave", "slave", str, filename, str1, str2, 0);

      execl("./slave", "slave", 0);

       //sleep(3);
       //exit(0);
    }
  }
  //printf("Main Process\n");

  //Parent process waiting for child processes to complete

  signal(SIGINT, SIGINT_handler);
  signal(SIGALRM, ALARM_handler);
  alarm(number_wait);
  /*for (i=0; i < number_proc; i++) {
    int status;
    wait(pids[i], &status, 0);
    *nano_seconds = *nano_seconds + 1;
    //printf("Process %d finished\n", pids[i]);
  }*/

 //loop that waits for 2 seconds in the simulated clcok or breaks after 100 processes
  while(*seconds < 2) {

    *nano_seconds = *nano_seconds + 200;
    //printf("%d\n", *seconds);
   //sleep(1);
    if(*nano_seconds >= 100000000) {

    *nano_seconds = *nano_seconds - 100000000;
    *seconds = *seconds + 1;


    }

    if (*seconds == 2)
      printf("2 simulated seconds reached.\n");

    if ((msgrcv(parent_queueid, &sentmsg, 1024, 1, IPC_NOWAIT)) >=0) {
        // perror("Failed to read message queue");
        // return 1;
        //printf("%s\n", sentmsg.mtext);
     mymsg.mtype = 2;
     sprintf(mymsg.mtext, "You can die");
//printf("%s\n", mymsg.mtext);
        if(msgsnd(parent_queueid, &mymsg, 1024,0) == -1) {
         perror("Failed write");
         return 1;
        }

        fprintf(logfile, "Master: Child pid is terminating at my time %d.%d because it reached %s in slave\n", *seconds, *nano_seconds, sentmsg.mtext);
        child_pid = fork();
        if (child_pid < 0) {

         printf("Could not fork!");
         return 1;

        }

        else if (child_pid > 0) {
          processes_made++;
        }

        else  {

    //printf("in child %d\n", getpid());
   //   execl("./slave", "slave", str, filename, str1, str2, 0);

      execl("./slave", "slave", 0);

       //sleep(3);
       //exit(0);
        }
    }

    if(processes_made == 100) {
      printf("100 processes made.\n");
      break;
    }

    waitpid(-1, 0, WNOHANG);
  }

 // kill(0,SIGTERM);

//kills all child processes after program has finished running
    kill_childs();

  //printf("%d\n", *nano_seconds);
  //detach and remove all shared memory and queues

  fclose(logfile);
  if (detachandremove(shm_id_seconds, seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }
  if (detachandremove(shm_id_nano_seconds, nano_seconds) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }

  if(removequeue(parent_queueid) < 0)
    perror("remove queue");
  if(removequeue(child_queueid) < 0)
    perror("remove queue");

//removequeue();

//removequeue();
  return 0;
}
