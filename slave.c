//slave
//This is the slave file for the oss/slave program
//Justin Henn
//Assignment 3
//3/05/17
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/msg.h>
#include<unistd.h>
#include "msgqueue.h"
#define PERM (S_IRUSR | S_IWUSR)

//structure for message

typedef struct {
   long mtype;
   char mtext[1024];
} mymsg_t;

/*void initqueue(int key, int queueid) {                     initialize the message queue 
   queueid = msgget(key, PERM);
   if (queueid == -1) {
    
      perror("msgget");
      exit(1);
   }
}*/

int main (int argc, char **argv) { 

  key_t key_seconds, key_nano_seconds, child_queue_key, parent_queue_key;
  key_seconds = 3423563;
  key_nano_seconds = 12345;
  child_queue_key = 85733;
  parent_queue_key = 922435;
  int* seconds;
  int* nano_seconds;
  //int proc_num = atoi(argv[1]);
  //int num_incr = atoi(argv[3]);
  //int number_of_proc = atoi(argv[4]);
  int shm_id_seconds, i, shm_id_nano_seconds, j;
  //FILE* logfile;
  //struct timespec tps;
  static int child_queueid, parent_queueid;
  int size;
  mymsg_t mymsg, sentmsg, parentmsg;
  srand(time(NULL));
  int full_nano = 0, timer = 0;

//connecto to queues

  child_queueid = initchildprocqueue(child_queue_key, child_queueid);

  parent_queueid = initchildprocqueue(parent_queue_key, parent_queueid);

  //get shared memory that was created in master
  if ((shm_id_seconds = shmget(key_seconds, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((seconds = (int *)shmat(shm_id_seconds, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    return 1;
  }


  if ((shm_id_nano_seconds = shmget(key_nano_seconds, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((nano_seconds = (int *)shmat(shm_id_nano_seconds, NULL, 0)) == (void *)-1) {
      perror("Failed to attach shared memory segment");
      return 1;
  }
 /*if(msgprintf(child_queueid, "This is process %d\n", getpid()) == -1 ) {
   perror("failed write");
    return 1;
 }*/

//get what the shared memory has an put it all in nano seconds then create a random number and add the shared memory to it

 full_nano = *seconds * 100000000;
 full_nano = full_nano + *nano_seconds;
 timer = rand() % 100000 + 1;
 timer = timer + full_nano;
 //printf("%d\n", timer);
 

 //for (i = 0; i < 1; i++) {

 //loop for message passing and waiting for the simulated timer to go past the random number the child is waiting for

 while(1) {

  sprintf(mymsg.mtext, "This is proc %d", getpid());
  int msg_len = strlen(mymsg.mtext) + 1;
  mymsg.mtype = 1;
 //printf("%s\n", mymsg.mtext);
  if ((msgread(child_queueid, sentmsg.mtext, 1024)) == -1) {
     perror("Failed to read message queue");
     return 1;
  }
  full_nano = *seconds * 100000000;
  full_nano = full_nano + *nano_seconds;    
 //  printf("%s received by %d\n", sentmsg.mtext, getpid());
 
   //sleep(2);
   //printf("HI this is pid %d\n", getpid());
  if (full_nano > timer) {

    sprintf(parentmsg.mtext, "%d.%d", *seconds, *nano_seconds);
    parentmsg.mtype = 1;
    //printf("%s\n", parentmsg.mtext);

    if(msgsnd(child_queueid, &mymsg, 1024,0) == -1) {
     perror("Failed write");
     return 1;
    }
    if(msgsnd(parent_queueid, &parentmsg, 1024,0) == -1) {
     perror("Failed write");
     return 1;
    }

    if ((msgrcv(parent_queueid, &sentmsg, 1024, 2, 0)) >=0) {
      //printf("pid, %s\n", sentmsg.mtext);
      break;
    }
   //sleep(2);
  }


  if(msgsnd(child_queueid, mymsg.mtext, 1024,0) == -1) {
    perror("Failed write");
    return 1;
  }
 }

return 0;
}
