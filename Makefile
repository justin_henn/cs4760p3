CC	= gcc		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET = oss slave

all: msgqueue.o oss slave

oss:	main.c
	$(CC) $(CFLAGS) -o oss main.c msgqueue.o

slave: slave.c
	$(CC) $(CFLAGS) -o slave slave.c msgqueue.o

msgqueue.o: msgqueue.c
	$(CC) $(CFLAGS) -c msgqueue.c

clean:
	/bin/rm -f *.o *~ *.txt *.out $(TARGET)
